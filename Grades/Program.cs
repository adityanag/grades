﻿using System;



namespace Grades
{
    class Program
    {
        static void Main(string[] args)
        {
           
            GradeBook book = new GradeBook();
            book.AddGrade(100f);
            book.AddGrade(89.5f);
            book.Name = "My Book";

            //GradeBook book2 = book;
            //book2.AddGrade(15f);

            GradeStatistics stats = book.ComputeStatistics();

            Console.WriteLine("Name:{0}",book.Name);
            Console.WriteLine("Average Grade:{0}",stats.AverageGrade);
            Console.WriteLine("Highest Grade:{0}", stats.HighestGrade);
            Console.WriteLine("Lowest Grade:{0}", stats.LowestGrade);

            



        }
    }
}
